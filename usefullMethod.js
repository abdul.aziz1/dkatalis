const exp = (a, b) => {
  return a ** b
}

const sqr = (a) => {
  return exp(a, 2)
}

const cube = (a) => {
  return exp(a, 3)
}

module.exports = {
  exp: exp,
  sqr: sqr,
  cube: cube
}
