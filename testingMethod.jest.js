const math = require('./usefullMethod')

test('Exponent function', () => {
    expect(math.exp(7, -5)).toBe(0.000059499018266198606)
})
test('Testing counter area of square', () => {
    expect(math.sqr(-2)).toBe(4)
})
test('Testing counter area of cube', () => {
    expect(math.cube(-2)).toBe(-8)
})
